# React Simple State

## Disclaimer:

This library is loosely based off [redux](https://redux.js.org/) using react's
new Context. It is not meant to be a comprehensive replacement, only something
I find slightly easier to use. While it works and I'm currently using it in a
few of my projects it should be considered alpha/beta quality at all times.

## Usage:

First create a store using:

```javascript
import { createStore } from "simple-state";

const initialState = { clicks: 0 };
const actionCreators = {
  click: state => ({ clicks: state.clicks + 1 })
};
const { Provider, connect, actions } = createStore(initialState, actionCreaters);
```

With a store created we can create our first component

```jsx
const { click } = actions;
const ClickView = ({ clicks }) => (
  <div>
    <div>The button has been clicked {clicks} time(s)</div>
    <button onClick={click}>Click</button>
  </div>
);

cosnt ClickViewContainer = connect(state => ({
  clicks: state.clicks
}))(ClickView);
```

And then we can tie it all together

```jsx
const App = () => (
  <Provider>
    <ClickViewContainer />
  </Provider>
);
```
