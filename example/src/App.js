import React from "react";
import { createStore } from "simple-state";
import "babel-polyfill";

const actionCreaters = {
  INCREMENT_COUNTER: state => ({ clicks: state.clicks + 1 }),
  very: {
    deeply: {
      nested: {
        DECREMENT_COUNTER: state => {
          if (state.clicks === 0) return { clicks: 0 };
          return { clicks: state.clicks - 1 };
        }
      }
    }
  }
};

const initialState = { clicks: 0 };

const { Provider, connect, actions } = createStore(initialState, actionCreaters);
console.log(actions);

const { incrementCounter } = actions;
const ClickView = props => {
  const handleIncrement = () => {
    incrementCounter();
  };

  const handleDecrement = () => {
    actions.very.deeply.nested.decrementCounter();
  };

  return (
    <div>
      <div>There have been {props.clicks} clicks</div>
      <button onClick={handleIncrement}>click me</button>
      <button onClick={handleDecrement}>and me</button>
    </div>
  );
};

const ClickViewContainer = connect(state => ({
  clicks: state.clicks
}))(ClickView);

const App = () => (
  <Provider>
    <ClickViewContainer />
  </Provider>
);

export default App;
