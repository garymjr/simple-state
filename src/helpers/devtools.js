let id = 0;

export default ({ initialState }, self) => {
  const reduxDevTools = window.devToolsExtension;

  const instanceID = id;
  id += 1;

  const name = `simple-state - ${instanceID}`;
  const features = { jump: true };

  const devTools = reduxDevTools.connect({ name, features });

  devTools.subscribe(data => {
    if (data.type === "START") {
      devTools.init(initialState);
    } else if (data.type === "RESET") {
      self.setState(initialState);
    } else if (data.type === "DISPATCH") {
      if (data.payload.type === "JUMP_TO_STATE" || data.payload.type === "JUMP_TO_ACTION") {
        self.setState(JSON.parse(data.state));
      }
    }
  });

  return (action, ...arg) => {
    devTools.send({ type: action, ...arg }, self.state, {}, instanceID);
  };
};
