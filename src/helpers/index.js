import React from "react";

export const createProvider = (setProvider, initialState, Provider) => {
  return class StateProvider extends React.Component {
    constructor(props) {
      super(props);

      this.state = initialState || {};
      setProvider(this);
    }

    render() {
      return <Provider value={this.state}>{this.props.children}</Provider>;
    }
  };
};

export const createConsumer = Consumer => mapStateToProps => WrappedComponent => {
  const RenderComponent = props => <WrappedComponent {...props} />;
  const ConnectedComponent = props => (
    <Consumer>
      {state => {
        const filteredState = mapStateToProps(state || {}, props || {});
        return React.cloneElement(<RenderComponent />, { ...props, ...filteredState });
      }}
    </Consumer>
  );

  RenderComponent.displayName = `Consumer(${WrappedComponent.displayName ||
    WrappedComponent.name ||
    "Unknown"})`;
  ConnectedComponent.displayName = `Connected(${WrappedComponent.displayName ||
    WrappedComponent.name ||
    "Unknown"})`;

  return ConnectedComponent;
};
