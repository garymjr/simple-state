import React, { createContext } from "react";
import { createProvider, createConsumer } from "./helpers";
import devtools from "./helpers/devtools";

const defaultMiddleware =
  process.env.NODE_ENV === "development" && typeof window !== undefined && window.devToolsExtension
    ? [devtools]
    : [];

export const createStore = (initialState = {}, actionCreators = {}, middleware = []) => {
  let provider;
  const setProvider = self => {
    const initializedMiddleware = [...middleware, ...defaultMiddleware].map(m => {
      return m({ initialState, actionCreators }, self, actions);
    });

    provider = {
      setState: (state, callback) => {
        return self.setState({ ...state }, callback);
      },
      initializedMiddleware
    };
  };

  let state = initialState;
  const setState = async (action, result, ...args) => {
    state = { ...state, ...result };
    await provider.setState(state, () => {
      provider.initializedMiddleware.forEach(fn => fn(action, ...args));
    });
  };

  const createActions = actionCreators => {
    return Object.keys(actionCreators).reduce((actions, action) => {
      if (typeof actionCreators[action] !== "function") {
        return {
          ...actions,
          [action]: {
            ...createActions(actionCreators[action])
          }
        };
      }

      let key = action
        .split("_")
        .map((n, i) => {
          if (i === 0) {
            return n.toLowerCase();
          }
          return n[0].toUpperCase() + n.slice(1).toLowerCase();
        })
        .join("");

      return {
        ...actions,
        [key]: (...args) => {
          let result = actionCreators[action](...args, state);
          return result.then
            ? result.then(result => setState(action.toUpperCase(), result, ...args))
            : setState(action.toUpperCase(), result, ...args);
        }
      };
    }, {});
  };

  const actions = createActions(actionCreators);
  const context = createContext();
  const Provider = createProvider(setProvider, initialState, context.Provider);
  const connect = createConsumer(context.Consumer);

  return { Provider, connect, actions, createActions };
};
