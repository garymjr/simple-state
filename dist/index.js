"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createStore = void 0;

var _react = _interopRequireWildcard(require("react"));

var _helpers = require("./helpers");

var _devtools = _interopRequireDefault(require("./helpers/devtools"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var defaultMiddleware = process.env.NODE_ENV === "development" && (typeof window === "undefined" ? "undefined" : _typeof(window)) !== undefined && window.devToolsExtension ? [_devtools.default] : [];

var createStore = function createStore() {
  var initialState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var actionCreators = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var middleware = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
  var provider;

  var setProvider = function setProvider(self) {
    var initializedMiddleware = _toConsumableArray(middleware).concat(defaultMiddleware).map(function (m) {
      return m({
        initialState: initialState,
        actionCreators: actionCreators
      }, self, actions);
    });

    provider = {
      setState: function setState(state, callback) {
        return self.setState(_objectSpread({}, state), callback);
      },
      initializedMiddleware: initializedMiddleware
    };
  };

  var state = initialState;

  var setState =
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(action, result) {
      var _len,
          args,
          _key,
          _args = arguments;

      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              for (_len = _args.length, args = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
                args[_key - 2] = _args[_key];
              }

              state = _objectSpread({}, state, result);
              _context.next = 4;
              return provider.setState(state, function () {
                provider.initializedMiddleware.forEach(function (fn) {
                  return fn.apply(void 0, [action].concat(args));
                });
              });

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    return function setState(_x, _x2) {
      return _ref.apply(this, arguments);
    };
  }();

  var createActions = function createActions(actionCreators) {
    return Object.keys(actionCreators).reduce(function (actions, action) {
      if (typeof actionCreators[action] !== "function") {
        return _objectSpread({}, actions, _defineProperty({}, action, _objectSpread({}, createActions(actionCreators[action]))));
      }

      var key = action.split("_").map(function (n, i) {
        if (i === 0) {
          return n.toLowerCase();
        }

        return n[0].toUpperCase() + n.slice(1).toLowerCase();
      }).join("");
      return _objectSpread({}, actions, _defineProperty({}, key, function () {
        for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments[_key2];
        }

        var result = actionCreators[action].apply(actionCreators, args.concat([state]));
        return result.then ? result.then(function (result) {
          return setState.apply(void 0, [action.toUpperCase(), result].concat(args));
        }) : setState.apply(void 0, [action.toUpperCase(), result].concat(args));
      }));
    }, {});
  };

  var actions = createActions(actionCreators);
  var context = (0, _react.createContext)();
  var Provider = (0, _helpers.createProvider)(setProvider, initialState, context.Provider);
  var connect = (0, _helpers.createConsumer)(context.Consumer);
  return {
    Provider: Provider,
    connect: connect,
    actions: actions,
    createActions: createActions
  };
};

exports.createStore = createStore;