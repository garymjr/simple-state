"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var id = 0;

var _default = function _default(_ref, self) {
  var initialState = _ref.initialState;
  var reduxDevTools = window.devToolsExtension;
  var instanceID = id;
  id += 1;
  var name = "simple-state - ".concat(instanceID);
  var features = {
    jump: true
  };
  var devTools = reduxDevTools.connect({
    name: name,
    features: features
  });
  devTools.subscribe(function (data) {
    if (data.type === "START") {
      devTools.init(initialState);
    } else if (data.type === "RESET") {
      self.setState(initialState);
    } else if (data.type === "DISPATCH") {
      if (data.payload.type === "JUMP_TO_STATE" || data.payload.type === "JUMP_TO_ACTION") {
        self.setState(JSON.parse(data.state));
      }
    }
  });
  return function (action) {
    for (var _len = arguments.length, arg = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      arg[_key - 1] = arguments[_key];
    }

    devTools.send(_objectSpread({
      type: action
    }, arg), self.state, {}, instanceID);
  };
};

exports.default = _default;